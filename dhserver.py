from flask import Flask, request, redirect, url_for, send_file
from dhkeygen import DiffieHellmanKeyExchange

app = Flask(__name__)


@app.route('/')
def index():
    response = 'open page /generator/module/pub_key and pass your values<br><a href="https://bitbucket.org/V__v/dhserver/src">sources</a>'
    return response

# parameters must be in hex string format and starts with 0x
@app.route('/<generator>/<module>/<pub_key>')
def secret_generate(generator, module, pub_key):
    hex_int_pub_key = int(pub_key, 16)
    
    DH = DiffieHellmanKeyExchange(module, generator)
    DH.generate_private_key(2048)
    DH.generate_public_key()
    DH.generate_secret(hex_int_pub_key)

    with open('public_key', 'w') as fpubkey:
        fpubkey.write(str(hex(DH.public_key)))
    with open('shared_secret', 'w') as fsecret:
        fsecret.write(str(hex(DH.shared_secret)))
    
    return 'open /result page for viewing shared secret. Yes, it is not secure :)'

@app.route('/result')
def view_generated():
    with open('shared_secret', 'r') as fsecret:
        return 'generated shared_secret:' + fsecret.read()    

@app.route('/get_pub_key')
def get_pub_key():
    with open('public_key', 'r') as fpubkey:
        return fpubkey.read()     

if __name__ == '__main__':    
    app.run(debug=True, host='0.0.0.0')
