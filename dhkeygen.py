# based on http://chrisvoncsefalvay.com/diffie-hellman-in-under-25-lines/
import hashlib
import ssl


class DiffieHellmanKeyExchange:
    def __init__(self, module, generator, key_length=2048):
        self.key_length = max(2048, key_length)
        self.prime = int(module, 16)
        self.generator = int(generator, 16)

    def generate_private_key(self, length):
       _rand = 0
       _bytes = length // 8 + 8
       while(_rand.bit_length() < length):
           _rand = int.from_bytes(ssl.RAND_bytes(_bytes), byteorder='big')
       self.private_key = _rand

    def generate_public_key(self):
        self.public_key = pow(self.generator, self.private_key, self.prime)

    def generate_secret(self, public_key):
        self.shared_secret = pow(public_key, self.private_key, self.prime)
        shared_secret_bytes = self.shared_secret.to_bytes(self.shared_secret.bit_length() // 8 + 1, byteorder='big')

        hash_alg = hashlib.sha256()
        hash_alg.update(bytes(shared_secret_bytes))
        self.key = hash_alg.hexdigest()
